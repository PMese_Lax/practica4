/* VectorOrdenado.java
 * Autor: Pablo Meseguer Lax
 * Fecha: 25/11/19
 * Descrp: Comprobar si el vector está ordenado.
 */
public class VectorOrdenado_8 {

	public static void main(String[] args) {
		int[] vector = { 1, 2, 3, 4, 5 };

		yaOrdenadoInt(vector);
	}

	static void yaOrdenadoInt(int[] vector) {
		int cont = 0;

		for (int i = 1; i < vector.length; i++) {
			if (vector[i] != (vector[i - 1] + 1)) { // Comprobar por parejas si son consecutivos
				System.out.println("False");
				break;
			} else {
				cont++;
			}
			if (cont == (vector.length - 1)) { // Si el vector se ha recorrido entero y hay continuidad devuelve True
				System.out.println("True");
			}

		}
	}
}