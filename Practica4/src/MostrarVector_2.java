
public class MostrarVector_2 {

	public static void main(String[] args) {
		int[] vector = new int[20];
		
		for(int i = 0; i < vector.length; i++) {
			vector[i] = i * 5;
		}
		
		for(int j = 0; j < vector.length; j++) {
			System.out.print(vector[j] + " ");
		}
	}

}
