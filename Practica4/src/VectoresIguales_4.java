/* Autor: Pablo Meseguer Lax
 * Fecha: 18/11/19
 * Descrp: Comprobar si dos vectores son iguales por tamaño y valores.
 */
import java.util.Scanner;

public class VectoresIguales_4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce el tamaño del primer vector: ");
		int n = input.nextInt();
		System.out.println("Introduce el tamaño del segundo vector: ");
		int m = input.nextInt();

		int[] vector1 = new int[n];
		int[] vector2 = new int[m];

		System.out.println("Introduce los valores del primer vector:");
		for (int i = 0; i < vector1.length; i++) {
			vector1[i] = input.nextInt();
		}

		System.out.println("Introduce los valores del segundo vector:");
		for (int j = 0; j < vector2.length; j++) {
			vector2[j] = input.nextInt();
		}

		vectoresIntIguales(vector1, vector2);
	}

	static void vectoresIntIguales(int[] vector1, int[] vector2) {
		int cont = 0;

		// Comprobar si los vectores son del mismo tamaño.
		if (vector1.length == vector2.length) {

			for (int i = 0; i < vector1.length; i++) {

				// Sumo a un contador el numero de veces que coinciden los valores de los vectores.
				if (vector1[i] == vector2[i]) {
					cont++;
				//En cuanto una pareja de valores es diferente, finalizo el programa.
				} else {
					System.out.println("Vectores diferentes. Fin del programa.");
					break;
				}
			}

			//Contador que contiene el numero de coincidencias, si es igual al numero de valores de un array, es verdadero.
			if (cont == vector1.length) {
				System.out.println("True");
			}
		} else {
			System.out.println("Vectores diferentes. Fin del Programa.");
		}
	}

}
