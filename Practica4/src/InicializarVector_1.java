
public class InicializarVector_1 {

	public static void main(String[] args) {
		int[] vector = new int[10];
		
		for(int i = 0; i < vector.length; i++) {
			vector[i] = 7;
		}
		//bucle for convencional
		System.out.println("Vector mostrado con un bucle for convencional...");
		for(int j = 0; j < vector.length; j++) {
			System.out.println(vector[j]);
		}
		System.out.println();
		
		//bucle for-each
		System.out.println("Vector mostrado con un bucle for-each...");
		for(int e : vector) {
			System.out.println(e);
		}
	}

}
