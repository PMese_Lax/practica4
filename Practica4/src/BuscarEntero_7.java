/* Autor: Pablo Meseguer Lax
 * Fecha: 25/11/19
 * Descrp: Encontrar num en vector.
 */
import java.util.Scanner;

public class BuscarEntero_7 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		int[] vector = { 1, 6, 4, 2, 7, 3, 1, 7, 3, 4, 0, 6, 2, 5, 8, 2, 4, 8, 2, 1, 6 };

		System.out.println("Introduce un número: ");
		num = input.nextInt();

		buscarInt(vector, num);
	}

	static void buscarInt(int[] vector, int num) {
		for(int i = 0; i < vector.length; i++) {
			if(vector[i] == num) {
				System.out.println("El indice de la primera coincidencia es: " + i);
				break;
			}
		}
	}

}
