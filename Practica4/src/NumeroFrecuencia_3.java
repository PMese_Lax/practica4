/* Autor: Pablo Meseguer Lax
 * Fecha: 18/11/19
 * Descrp: Frecuencia de un numero en un vector.
 */

import java.util.Scanner;

public class NumeroFrecuencia_3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		int[] vector = new int[10];
		
		// Inicialización del vector.
		System.out.println("Introduce 10 valores:");

		for (int i = 0; i < vector.length; i++) {
			vector[i] = input.nextInt();
		}
		
		// Número a saber su frecuencia.
		System.out.println("Introduce el número para saber su frecuencia:");
		num = input.nextInt();
		
		// Llamada al método.
		frecuenciaNumero(vector, num);
	}

	static void frecuenciaNumero(int[] vector, int num) {
		int cont = 0;
		
		// Bucle for para comprobar cada valor del vector.
		for(int i = 0; i < vector.length; i++) {
			if(vector[i] == num) {
				cont++;
			}
		}
		System.out.println("La frecuencia del número recibido en el vector es: " + cont);
	}

}
