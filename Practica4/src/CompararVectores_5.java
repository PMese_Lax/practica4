/* Autor: Pablo Meseguer Lax
 * Fecha: 18/11/19
 * Descrp: Comparar tamaño de vectores.
 */

public class CompararVectores_5 {

	public static void main(String[] args) {
		char[] vector1 = {'a', 'b', 'c'};
		char[] vector2 = {'a', 'b'};
		
		compararVectoresChar(vector1, vector2);
	}
	static void compararVectoresChar(char[] vector1, char[] vector2) {
		if(vector1.length == vector2.length) { //si los vectores son iguales imprime un 0
			System.out.println("0");
		}else if(vector1.length > vector2.length) { //si el primer vector es mayor imprime un 1
			System.out.println("1");
		}else { //si el segundo vector es el mayor imprime un -1
			System.out.println("-1");
		}
	}
}
