/* ContarConsecutivos.java
 * Autor: Pablo Meseguer Lax
 * Fecha: 18/11/19
 * Descrp: Numeros consecutivos.
 */
public class ContarConsecutivos_6 {

	public static void main(String[] args) {
		int[] vector = { 1, 2, 3, 5, 3, 4, 5, 6, 7, 5, 4, 5, 7, 8 };

		contarIntConsecutivos(vector);
	}

	static void contarIntConsecutivos(int[] vector) {
		int cont = 1;
		int contMax = 0;

		for (int i = 1; i < vector.length; i++) {
			// Si el valor actual es consecutivo al anterior, cont++
			if (vector[i] == (vector[i - 1] + 1)) {
				cont++;
				// Si el valor de cont es mayor o igual que la variable máxima alcanzada lo
				// guardo en contMax.
				if (cont >= contMax) {
					contMax = cont;
				}
				// Cada vez que se rompa la serie de consecutivos reinicio el contador.
			} else {
				cont = 1;
			}
		}
		System.out.println("Cantidad de números consecutivos: " + contMax);
	}
}
